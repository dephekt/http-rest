"""A package providing generic HTTP request functionality."""
from typing import Union, Optional, Tuple, IO, Callable, MutableMapping, Any, Mapping, Iterable
import logging
import urllib.parse

from requests import PreparedRequest, Response, Request, Session
from requests.adapters import HTTPAdapter, BaseAdapter
from requests.cookies import RequestsCookieJar
from requests.auth import AuthBase

DEFAULT_TIMEOUT = (3.05, 27)


class TimeoutHttpAdapter(HTTPAdapter):
    """Request adapter enforcing a default timeout on all requests."""

    def __init__(self, *args, **kwargs):
        self.timeout = DEFAULT_TIMEOUT

        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]

        super().__init__(*args, **kwargs)

    def send(self, request: PreparedRequest, **kwargs) -> Response:  # pylint: disable=arguments-differ
        """Send a given :class:`PreparedRequest`."""
        timeout = kwargs.get("timeout")

        if timeout is None:
            kwargs["timeout"] = self.timeout

        return super().send(request=request, **kwargs)


class BaseUrlSession(Session):
    """A Requests session with base URL support.

    This is identical to :class:`requests.Session` except it supports
    specifying a base URL to be prepended to all requests which have
    no HTTP(S) schema in their URL.

    If no base_url is given, the behavior is identical to a
    :class:`requests.Session`.
    """

    def __init__(
        self,
        base_url: Optional[str] = None,
    ):
        self.base_url = base_url
        super().__init__()

    def request(
        self,
        method: str,
        url: Union[str, bytes],
        params: Union[None, bytes, MutableMapping[str, Any]] = None,
        data: Union[None, str, bytes, Mapping[str, Any], Iterable[Tuple[str, Optional[str]]], IO] = None,
        headers: Optional[MutableMapping[str, str]] = None,
        cookies: Union[None, MutableMapping[str, str], RequestsCookieJar] = None,
        files: Optional[MutableMapping[str, IO]] = None,
        auth: Union[None, Tuple[str, str], AuthBase, Callable[..., PreparedRequest]] = None,
        timeout: Union[None, float, Tuple[float, float], Tuple[float, None]] = None,
        allow_redirects: Optional[bool] = True,
        proxies: Optional[MutableMapping[str, str]] = None,
        hooks: Optional[
            MutableMapping[str, Union[Iterable[Callable[[Response], Any]], Callable[[Response], Any]]]
        ] = None,
        stream: Optional[bool] = None,
        verify: Union[None, bool, str] = None,
        cert: Union[None, str, Tuple[str, str]] = None,
        json: Optional[Any] = None,
    ) -> Response:
        """Construct a :class:`Request`, prepare it and send it.

        The parameters here are identical to the `request()` method in
        the base :class:`Session` class.

        The only difference here is we prepend a base URL to any request
        which does not have a detectable scheme.
        """
        if self.base_url and not urllib.parse.urlparse(url).scheme:
            url = urllib.parse.urljoin(self.base_url, url)

        req = Request(
            method=method.upper(),
            url=url,
            headers=headers,
            files=files,
            data=data or {},
            json=json,
            params=params or {},
            auth=auth,
            cookies=cookies,
            hooks=hooks,
        )
        prep = self.prepare_request(req)

        proxies = proxies or {}

        settings = self.merge_environment_settings(prep.url, proxies, stream, verify, cert)

        send_kwargs = {"timeout": timeout, "allow_redirects": allow_redirects}
        send_kwargs.update(settings)
        resp = self.send(prep, **send_kwargs)

        return resp


class SaneRestClient:
    """A basic REST API client with sane defaults.

    HTTP Basic Authentication and HTTP bearer token authentication
    are currently supported, as well as no authentication.

    You can extend this class to support additional authentication
    schemes by subclassing it, modifying __init__() to your needs
    and adding a new authentication method (or methods).

    This client class supports providing default request timeouts
    (recommended for all production code), configuring a base URL to be prepended
    to every given request URL, as well as overriding those defaults
    on a per-request basis. For leveraging this, create a
    :class:`requests.Request` object and pass it to the `request()`
    method of :class:`SaneRestClient`. For more info see the `request()` method.

    It also supports pluggable HTTP transport adapters by passing
    them to ``transport=<adapter>``.
    """

    def __init__(
        self,
        base_url: Optional[str] = None,
        session: Optional[Session] = None,
        headers: Optional[MutableMapping[str, str]] = None,
        timeout: Union[None, float, Tuple[float, float], Tuple[float, None]] = DEFAULT_TIMEOUT,
        adapter: Optional[BaseAdapter] = None,
        bearer_token: Optional[str] = None,
        auth: Union[None, Tuple[str, str], AuthBase, Callable[..., PreparedRequest]] = None,
    ):
        self.bearer_token = bearer_token
        self.base_url = base_url
        self.timeout = timeout
        self.auth = auth

        if headers:
            self.headers = headers
        else:
            self.headers = {}

        if adapter:
            self.adapter = adapter
        else:
            self.adapter = TimeoutHttpAdapter(timeout=self.timeout)

        if session:
            self.session = session
        else:
            self.session = BaseUrlSession(base_url=self.base_url)
            if self.adapter:
                self.session.mount(prefix="https://", adapter=self.adapter)
                self.session.mount(prefix="http://", adapter=self.adapter)
            self.session.headers.update(self.headers)
            self.session.hooks["response"] = [self._log_headers_debug, self._assert_status_hook]
            if self.auth:
                self.create_session_basic_auth(self.auth)
            elif self.bearer_token:
                self.create_session_token_auth(self.bearer_token)

    def create_session_basic_auth(self, auth: Union[None, Tuple[str, str], AuthBase, Callable[..., PreparedRequest]]):
        """Inject the authentication token into the session."""
        self.session.auth = auth

    def create_session_token_auth(self, token: str):
        """Inject a bearer token into the Authorization header for every request."""
        self.session.headers["Authorization"] = f"Bearer {token}"

    @staticmethod
    def _assert_status_hook(response: Response, *args, **kwargs):  # pylint: disable=unused-argument
        """Event hook to check response status after every request."""
        # args and kwargs are allowed even if we don't use them
        # otherwise request hook will fail as it sends args like
        # timeout and others that were passed with the request.
        response.raise_for_status()

    @staticmethod
    def _log_headers_debug(response: Response, *args, **kwargs):  # pylint: disable=unused-argument
        """Event hook to log response headers if debug logging is enabled."""
        logging.debug("Response Headers: %s", response.headers)
