# http-rest

An implementation of the Python Requests library using sane defaults out of the box and ready to be extended for quickly creating REST API clients.

## Summary

There are three classes provided:

- TimeoutHttpAdapter(requests.adapters.HTTPAdapter)
- BaseUrlSession(requests.Session)
- SaneRestClient

### TimeoutHttpAdapter

This subclass of HTTPAdapter overrides the parent's `__init__()` and `send()` methods to implement a default, mandatory timeout on all requests. By default, the requests library does not apply any timeout, so in the event of a misbehaving web server, a request could end up blocking indefinitely. For that reason, the maintainers recommend all production code utilize timeouts.

This is tedious, prone to user error, and requires a lot of repetitive code. So this adapter provides a mechanism where a default timeout is always applied. Out of the box, it will wait for 3.05 seconds for your client to establish a connection to a remote machine and 27 seconds for the client to wait for the server to send a response, e.g. the `connect` and `read` timeouts, respectively.

These default values can be override both when instantiating `TimeoutHttpAdapter` and on a per-request basis, by specifying a `timeout=` argument to either.

### BaseUrlSession

This subclass of requests.Session is identical to its parent except that it supports specifying a base URL which will be prepended to all requests that do not include an HTTP(S) schema in their URL. This is done by overriding the default `requests.Session().request()` method.

For example:

```python
from http_rest import BaseUrlSession

s = BaseUrlSession(base_url="https://api.domain.net/v1")
response = s.get(url="/employees").json()
```

This would result in a GET request to https://api.domain.net/v1/employees.

You can choose not to provide a base URL when instantiating the session at which point the behavior will be identical to a normal requests.Session object.

You can also override the base URL on a per-request basis by specifying a complete URL in your request. For example:

```python
from http_rest import BaseUrlSession

s = BaseUrlSession(base_url="https://api.domain.net/v1")
data = {"client_id": "client_id", "api_token": "api_token"}
response = s.post(url="https://auth.api.domain.net/oauth2/token", json=data).json()
```

In this case, the POST request would go to the given URL of https://auth.api.domain.net/oauth2/token and no base URL would be applied.

### SaneRestClient

This class brings together the previous mentioned classes (TimeoutHttpAdapter and BaseUrlSession) into a base API client class that can be quickly subclassed and extended to work with (hopefully) any API by adding additional methods. It can also easily be extended to support additional authentication methods. Take a look at the source code to get familiar with how it's implemented.

You can pass a base_url and a default timeout when instantiating it and those will be passed to the default BaseUrlSession and TimeoutHttpAdapter utilized by the client.

For example:

```python
from http_rest import SaneRestClient
client = SaneRestClient(base_url="https://api.domain.net/v1", timeout=(10, 30), bearer_token="my-api-key")
response = client.session.get("/employees").json()
```

In this case, the client will create a BaseUrlSession at `client.session` with the given base URL and will pass `(10, 30)` to the `TimeoutHttpAdapter` at `client.adapter` and automatically mount that adapter to the `http://` and `https://` prefixes of `client.session`.

It will also automatically set `client.bearer_token` to `"my-api-key"` and set the `"Authorization: Bearer {api_key}"` header in the `client.session`, so there's no need to manually set headers in this particular use case.

This same behavior works with HTTP basic authentication as well, e.g.:

```python
from requests.auth import HTTPBasicAuth
from http_rest import SaneRestClient
client = SaneRestClient(
    base_url="https://api.domain.net/v1",
    timeout=(10, 30),
    auth=HTTPBasicAuth("username", "password")
)
response = client.session.get("/employees").json()
```

#### Raise for Status

The SaneRestClient also calls `response.raise_for_status()` on every response without your having to remember to manually specify it after every request. It does this by implementing Requests' event hooks functionality. There's a built-in method here called `_assert_status_hook` that gets called by the session's response hook which then calls raise_for_status() on the response object.

If you don't want this behavior, after instantiating the client, you can do `client.session.hooks["response"] = []` to remove the event hooks.


#### Debug logging of response headers

If you have debug logging enabled, the response headers from each request will be logged automatically. You can disable this by either disabling debug logging or removing the event hook like described above.

The default event hooks are [self._log_headers_debug, self._assert_status_hook]. You can remove one, both, or add more as required.
